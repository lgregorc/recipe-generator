import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

from keras.models import Sequential
from keras.layers import InputLayer, LSTM, Dense
from keras.callbacks import ModelCheckpoint
from keras.losses import mse
from keras.metrics import mse
from keras.optimizers import Adam


n = 10001
per = 11
blur = .1

slide_n = 16

epochs = 10
learning_rate = .0001
batch_size = 64

rng = np.random.default_rng()
t = np.linspace(0, per * 2 * np.pi, n)
f1 = np.sin(t) + (rng.random(n) - .5) * blur


X = np.lib.stride_tricks.sliding_window_view(f1[:-1], slide_n)
y = f1[slide_n:]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)

print(X_train.shape, y_train.shape, X_test.shape, y_test.shape)

# print(X_train[0], y_train[0])

model = Sequential()
model.add(InputLayer((slide_n, 1)))
model.add(LSTM(64))
model.add(Dense(16, "relu"))
model.add(Dense(1, "linear"))

model.summary()

model.compile(loss=mse, optimizer=Adam(learning_rate=learning_rate), metrics=mse)
model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size)  # validation_data=(X_test, y_test)

#model.save("model_00.h5")

pred = model.predict(X)
print(pred.shape)


plt.plot(y[:(2 * n // per)], c="#00aa33")
plt.plot(pred[:(2 * n // per)], c="#aa3300")
plt.show()
