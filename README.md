# recipe generator
A repository for generating a recipe based on an ingredient list.
Three approaches were used:
- similarity matrix
- RNN with LSTM model
- Markov Chain

## The recipe objects don't contain data on how to cook the recipe, since this project's results are not cookable!
> /recipe_acquire/results


## Webscraper for Chefkoch.de
Example/How to use
```python
#get all available dish categories
categories = ChefKochAPI.get_categories()

#parse all recipes (yields) from the first category
recipes = ChefKochAPI.parse_recipes(categories[0])

#write recipes to json file one at a time
DataParser.write_recipes_to_json(category.title, recipes)

```

## RNN with LSTM 
How to use it:
```python
# converts json to df (10000 recipe in one file)
recipe_aquire/convert.py 

# analysis of ingredients
recipe_aquire/ingredients_analysis.ipynb

# analysis of duplicates
recipe_aquire/duplicates_detector.ipynb

# train  and use model
recipe_aquire/tryout.ipynb
```