import numpy as np
import pandas as pd
import os
import re

split_pattern = r"[,(0123456789 ]"  # add char(s) if you find some disturbance in the force
path_ing = "recipes/converted/"
path_savefile = "/home/grupp3/project3/Chefkoch-API/data/ingredient_count_sortnr.csv"  # so!
#path_savefile = "\\wsl.localhost/Ubuntu/home/grupp3/project3/Chefkoch-API/data/ingredient_count.csv"  # NICHT so!
ing_list = []  

for file in os.listdir(path_ing):
    if file[:3] == "ing" and file[-7:] == "feather":
        df_load = pd.read_feather(path_ing + file)
        print(f"adding file: {file}, {df_load.shape}")

        for index, row in df_load.iterrows():
            if index < 999999:  # use small int for debugging
                for ingredients in row:
                    try:
                        ingredient = re.split(pattern=r"[|]", string=ingredients)[0].lower()  # separate ingredient|unit|ammount
                        ingredient_plain = re.split(pattern=split_pattern, string=ingredient)[0]
                        # print(index, ingredient_plain)  # only for debugging!
                        if len(ingredient_plain) > 0:
                            ing_list.append(ingredient_plain)

                    except:  # time is of the essence ;-)
                        pass


ingredients, counts = np.unique(np.asarray(ing_list), return_counts=True)
print(f"total number of ingredients: {len(ing_list)}")
ing_df = pd.DataFrame({"ingredient": ingredients, "count": counts}).sort_values(by=["count"])
print(ing_df)

ing_df.to_csv(path_savefile, index=False)
