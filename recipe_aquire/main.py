from chefkoch import ChefKochAPI, DataParser
from datetime import date
import glob, os

if __name__ == '__main__':
    categories = ChefKochAPI.get_categories()

    for category in categories:
        print(str(category))
    foldername = 'recipes_without_duplicates'


    excluded_categories = ['Getränke', 'Alkoholfrei', 'Longdrink', 'Kaffee, Tee  Kakao', 'Likör', 'Bowle', 'Cocktail', 'Punsch', 'Shake']
    for category in categories[0:]:
        category_path_name = category.title.replace(" ", "-").replace("/","-")
        if category.title not in excluded_categories and len(glob.glob(foldername + '/*-category-' + category_path_name + ".json")) == 0:
                category_recipes = ChefKochAPI.parse_recipes(category)
                print(category)
                DataParser.write_recipes_to_json(foldername + '/' + str(date.today()) + "-category-" + category_path_name, category_recipes)
