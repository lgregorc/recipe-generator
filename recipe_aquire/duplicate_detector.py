import pandas as pd
import glob, os 


files = 'recipe_df_*.feather'


os.chdir("recipes/converted")
recipe_df = pd.DataFrame()


for file in glob.glob(files):
    try:
        recipe_df = pd.concat(recipe_df, pd.read_feather(file))
    except Exception as e:
        print('Cant read file: ' + file)
        print(e)

