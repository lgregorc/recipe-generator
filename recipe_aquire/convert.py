import glob, os 
import json
import pandas as pd
import pyarrow

files = '2023-02-*.json'

if __name__ == '__main__':
    ingredients = pd.DataFrame()
    os.chdir("recipes_without_duplicates")

    recipe_df = pd.DataFrame() #columns=['recipe_name', 'instructions', 'kcal', 'rating', 'rating_amount', 'category', 'teaser'])
    ingredients_df = pd.DataFrame()
    error_file = open("errors.txt", "w+")
    count_recipes = 0
    count_total = 0
    try:
        for file in glob.glob(files):
            print(file)
            try:
                df = pd.read_json(file)
            except (ValueError, KeyError) as e:
                   print(e)
                   error_file.write('damaged content: ' + str(file) + "\n")

            df.dropna(inplace=True)
            # remove not needed columns and convert others
            if 'images' in df.columns:
                df.drop('images', axis = 1, inplace=True)

            # convert category, sometimes it is already converted
            if 'category' in df.columns and type(df['category'][0]) == 'dict':
                df['category'] = df['category'].apply(lambda row: row['title'])
            df['tags'] = df['tags'].apply(lambda row: ','.join(row))
            
            # ingredients saved seperately, id as foreign key
            ingredients = pd.json_normalize(df['ingredients'])
            for col in ingredients.columns:
                ingredients[col] = ingredients[col].apply(lambda ing: ing['name'] + '|' + ing['amount'] + '|' + ing['unit'] if ing != None else pd.NA)
            ingredients['id'] = df['id']
            
            recipe_df = pd.concat([recipe_df, df]).reset_index(drop=True)
            ingredients.columns = ingredients.columns.astype(str)
            ingredients_df = pd.concat([ingredients_df, ingredients]).reset_index(drop=True)
                     
            # save when above 100.000 
            count_recipes += len(recipe_df)
            if count_recipes > 100000:
                count_total += count_recipes
                recipe_df.to_feather('converted/recipe_df_' + str(count_total) + '.feather')
                ingredients_df.to_feather('converted/ingredients_df_' + str(count_total) +  '.feather')
                recipe_df.to_csv('converted/recipe_df_' + str(count_total) +  '.csv')
                ingredients_df.to_csv('converted/ingredients_df_' + str(count_total) +  '.csv')
                recipe_df = pd.DataFrame()
                ingredients_df = pd.DataFrame()
                count_recipes = 0

    except json.decoder.JSONDecodeError:
        print('Cant read file: ' + file)
        error_file.write('damaged file: ' + file + "\n")
    #print(recipe_df)
    recipe_df.to_feather('converted/recipe_df_'+ str(count_total) +  '.feather')
    ingredients_df.to_feather('converted/ingredients_df_' + str(count_total) +  '.feather')
    recipe_df.to_csv('converted/recipe_df_' + str(count_total) + '.csv')
    ingredients_df.to_csv('converted/ingredients_df_' + str(count_total) +  '.csv')
    error_file.close()
    print("Done importing ", str(count_total), " recipes")



