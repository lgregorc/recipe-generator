import pandas as pd
import glob, os 

files = 'ingredients_df_147013.feather'

os.chdir("recipes/converted")
ingredients_df = pd.DataFrame()
error_file = open("errors.txt", "w+")

for file in glob.glob(files):
    try:
        ingredients_df = pd.read_feather(file)
        ingredients_df = ingredients_df.loc[:,ingredients_df.columns != 'id']
        simple_df = pd.DataFrame(columns=['ingredient', 'amount', 'unit'])
        for col in ingredients_df.columns:
            print(ingredients_df[col].apply(lambda row: row.split('|') if row != None and not pd.isna(row) else [pd.NA,pd.NA,pd.NA]).toList())
            simple_df = pd.concat([simple_df,pd.DataFrame(ingredients_df[col].apply(lambda row: row.split('|') if row != None and not pd.isna(row) else [pd.NA,pd.NA,pd.NA]))])

        print(simple_df)
    except Exception as e:
        print('Cant read file: ' + file)
        print(e)
        error_file.write('damaged file: ' + file + "\n")

# bereinigung
mapping = {
    'Zwiebel(n)': 'Zwiebel',
    'Kartoffel(n)': 'Kartoffel'
}
